/* Return TRUE/FALSE base on windows size.
========================================================================== */
const isSmallScreen = () => {
  return window.innerWidth <= 1260
}

/* ==========================================================================
  Collapse funtionality
  ========================================================================== */
const collapseSidebarOnSmallScreen = () => {
  if (isSmallScreen()) {
    // in small screen the menu starts collapsed
    $('.js-main-menu').addClass('collapsed-sidebar')
    $('.js-footer-content').addClass('display-collapsed')
    // return true to initiate the tooltip
    window.localStorage.setItem('collapsedSidebar', $('.js-main-menu').hasClass('collapsed-sidebar'))
    return true
  }
}

const collapseSidebarBasedOnLocalStorage = () => {
  if (window.localStorage.getItem('collapsedSidebar') === 'true') {
    // if saved in locastorage that collapsedSidebar=true, menu starts collapsed
    $('.js-main-menu').addClass('collapsed-sidebar')
    // return true to initiate the tooltip
    return true
  }
}

const toggleSidebarAndSave = () => {
  $('.js-main-menu').toggleClass('collapsed-sidebar')
  $('.js-footer-content').toggleClass('display-collapsed')

  toggleTooltip()

  if (!isSmallScreen()) {
    saveSidebarState()
  }
}

const autoToggleSidebar = () => {
  const shouldCollapse = isSmallScreen() || window.localStorage.getItem('collapsedSidebar') === 'true'

  toggleTooltip()

  $('.js-main-menu').toggleClass('collapsed-sidebar', shouldCollapse)
  $('.js-footer-content').toggleClass('display-collapsed', shouldCollapse)
}

const saveSidebarState = () => {
  window.localStorage.setItem('collapsedSidebar', $('.js-main-menu').hasClass('collapsed-sidebar'))
}

// Initiate or destroy the tooltip according to the state of the collapsible menu
const toggleTooltip = () => {
  // if the menu is not collapsed (meaning, it is open) we dont need to show the tooltips
  if ($('.collapsed-sidebar').length === 0) {
    $('.js-sidebar-tooltip').attr('data-original-title', 'Collapse menu')
  } else {
    // If sidebar is collapsed update sidebar toggle btn tooltip title
    $('.js-sidebar-tooltip').attr('data-original-title', 'Open menu')
  }
}

// Togle on resize and click.
$(window).resize(autoToggleSidebar)
$(document).on('click', '.js-sidebar-toggle', toggleSidebarAndSave)

// When the page is done loading, check if we should toggle the menu based on width or localstorage
$(document).ready(() => {
  if (window.localStorage.getItem('collapsedSidebar') === 'true') {
    // Always display content-footer if showSidebar === false
    $('.js-footer-content').addClass('display-collapsed')
    // If sidebar is collapsed update sidebar toggle btn tooltip title
    $('.js-sidebar-tooltip').attr('data-original-title', 'Open menu')
  }
  // If sidebar isn't collapsed update sidebar toggle btn tooltip title
  if (window.localStorage.getItem('collapsedSidebar') === 'false') {
    $('.js-sidebar-tooltip').attr('data-original-title', 'Collapse menu')
  }

  // if either the screen is small or it is saved in localstorage to keep the menu open
  // then we Initiate the tooltip
  if (collapseSidebarOnSmallScreen() || collapseSidebarBasedOnLocalStorage()) {
    toggleTooltip()
  }

  /* ==========================================================================
  Menu collapse funtionality for mobile
  ========================================================================== */
  /* Select the mobile menu elements */
  const mobileMenu = $('.js-mobile-menu').find('nav')
  /* Toggle mobile menu on click */
  $('.js-burger-menu').click(() => {
    mobileMenu.slideToggle()
    $('body').toggleClass('mobile-menu-open')
  })

  showCollpasedMenuFloatingItems()
})

/* Find if an element inside the menu dropdown list is active and leave the dropdown open */
$(window).on('load', function () {
  /* Big screen dropdown open */
  const $selectedSub = $('.menu-dropdown').find('.selected')[0]
  $($selectedSub).parents('.menu-dropdown').addClass('selected')
  $($selectedSub).parents('input').prop('checked', true)
  $($selectedSub).parent().parent().siblings('input').prop('checked', true)

  /* Mobile dropdown open */
  const $menuDropdownMobile = $('.js-mobile-menu .menu-dropdown')
  const $selectedSubMobile = $('.js-mobile-menu .menu-dropdown').find('.selected')[0]

  for (let i = 0; i < $menuDropdownMobile.length; i++) {
    $($selectedSubMobile).parents('.menu-dropdown').addClass('selected')
    $($menuDropdownMobile[i]).find('input').attr('id', `internal-tools-toggle-mobile-${i}`)
    $($menuDropdownMobile[i]).find('label').attr('for', `internal-tools-toggle-mobile-${i}`)
    $($selectedSubMobile).parent().parent().siblings('input').prop('checked', true)
  }
})

const showCollpasedMenuFloatingItems = () => {
  $('.menu-item').on('mouseover', function () {
    const $menuItem = $(this)
    const $collapsedTitle = $('> .menu-element', $menuItem)

    const menuItemPos = $menuItem.position()

    // nav height to position dropdown menu from bottom
    const navHeight = $('.nav-wrap').height()
    const finalDropdownPos = navHeight - menuItemPos.top

    // Dropdown height and calculate bottom height + extra px for margin
    const windowHeight = $('.main-menu').height()
    const dropdownHeight = $('> .menu-element', $menuItem).height()
    const bottomHeight = Math.round(windowHeight - menuItemPos.top) - 70

    if (dropdownHeight < bottomHeight) {
      $collapsedTitle.css({
        'bottom': 'auto',
        'top': menuItemPos.top,
        'left': $menuItem.hasClass('menu-dropdown') ? menuItemPos.left + Math.round($menuItem.outerWidth() * 1) : 0
      })
    } else {
      $collapsedTitle.css({
        'bottom': $menuItem.hasClass('menu-dropdown') ? finalDropdownPos - 37 : finalDropdownPos,
        'top': 'auto',
        'left': $menuItem.hasClass('menu-dropdown') ? menuItemPos.left + Math.round($menuItem.outerWidth() * 1) : 0
      })
    }
  })
}
