function getLogoIconsService (callback) { // eslint-disable-line no-unused-vars
  $.when(
    $.ajax({
      url: '/api/logo-icons/',
      dataType: 'json',
      error: function (xhr, status, error) {
        console.error(`There was an error in the request: ${error}`)
      }
    }))
    .then(function (logoIconsResponse) {
      callback(logoIconsResponse)
    })
}
