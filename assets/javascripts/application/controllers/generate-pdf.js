$(function () {
  generatePdf()
})

/* use html2pdf lib to generate PDF from a div. */
const generatePdf = () => {
  /* Select the div that would be printed */
  const element = $('.js-wrap-to-print')

  /* Generate the PDF on click. */
  $('.js-generate-pdf').click(() => {
    /* eslint-disable */
    html2pdf(element[0])
    /* eslint-enable */
  })
}
