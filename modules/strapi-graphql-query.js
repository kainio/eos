const axios = require('axios')
const fs = require('fs')


/* Read the data from the config file */
const readConfigFile = async () => {
  try {
    const readFile = fs.readFileSync('config.json', (err, data) => {
      if (err) {
        console.log('ERROR => readConfigFile(): ', err);
      }

      return data
    })

    return await JSON.parse(readFile)
  } catch (error) {
    console.log('error: ', error);
  }
}


const strapiGraphql = async query => {
  /* Get the data from config file */
  const readConfiFile = await readConfigFile()

  /* Destructuring the object */
  const { useStrapiAs, envVars: { dev, prod } } = readConfiFile

  const strapiRequestConfig = {
    url: useStrapiAs === 'dev' ? dev.strapiUrl : prod.strapiUrl,
    user: useStrapiAs === 'dev' ? dev.user : prod.user,
    pass: useStrapiAs === 'dev' ? dev.pass : prod.pass
  }

  try {
    /* Auth request
      ========================================================================== */
    /* Sends a requests to the auth with username and password */

    const { url, user, pass } = strapiRequestConfig

    const authRequests = await axios.post(`${url}/auth/local`, {
      identifier: user,
      password: pass
    })

    /* If credentials are correct, returns the JWT */
    const { data: { jwt } } = await authRequests

    /* Data request
      ========================================================================== */
    /* Request the data from server passing the Autorization header with the bearer token*/
    const request = await axios.get(`${url}/graphql?query={${query}}`, {
      headers: {
        Authorization: `Bearer ${jwt}`
      }
    })

    const { data } = request

    return data
  } catch (error) {
    console.log('ERROR => strapiGraphql(): ', error);
  }
}

module.exports = {
  strapiGraphql,
  readConfigFile
}
