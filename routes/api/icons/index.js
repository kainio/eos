const express = require('express')
const router = express.Router()
const fs = require('fs')

/* ==========================================================================
   Generate a single .json file from all the eos-icons files.
   ========================================================================== */

const iconsSet = JSON.parse(fs.readFileSync('assets/javascripts/application/models/eos-icons.json', 'utf8'))

/* ==========================================================================
  API ENDPOINT
========================================================================== */
/**
 *  => /api/icons                 Return all the icons
 *  => /api/icons/SEARCH          Return one of more icons based on name, tag or do/dont
 */
/* Show all icons if no query is present
  ========================================================================== */
router.get('/', (req, res, next) => {
  res.status(200).json(iconsSet)
})

/* Return a parced response object
  ========================================================================== */
router.get('/:search', (req, res, next) => {
  const reqLower = req.params.search.toLocaleLowerCase()
  const multipleWordsQuery = reqLower.split(' ')
  /* ==========================================================================
     Temporary (_) array that holds our request response.
     ========================================================================== */
  const _response = []
  // Update array with the item position that match the query
  const upadateResponse = (i) => {
    _response.push(iconsSet[i])
  }

  /* ==========================================================================
    Funtion to filter content.
    ========================================================================== */
  const filterIcons = (search) => {
    for (let i = 0; i < iconsSet.length; i++) {
      // Create a new array with words from the icons. Ej: eos_chains = ['eos', 'chains']
      const _arrWords = iconsSet[i].name.split(['_'])
      const selectTags = iconsSet[i].tags

      // Check for tags
      for (let y = 0; y < selectTags.length; y++) {
        // Split the multiple words tag in an array of words.
        const iconTags = selectTags[y].split(/\s/)
        // If the word is present in one of the tag, show that icon.
        if (iconTags.find(tag => tag === search)) {
          upadateResponse(i)
        }
      }

      // Check the do for each icon, clear the HTML from the string and convert everthing to lowercase.
      const _do = iconsSet[i].do.replace(/<[^>]+>/ig, '')
      _do.split(/\s+|\./ig)
      const iconDo = _do.toLocaleLowerCase()

      // Check the dont for each icon, clear the HTML from the string and convert everthing to lowercase.
      const _dont = iconsSet[i].dont.replace(/<[^>]+>/ig, '')
      _dont.split(/\s+|\./ig)
      const iconDont = _dont.toLocaleLowerCase()

      // If the icon title, do or dont match the query, display icons.
      for (let y = 0; y < _arrWords.length; y++) {
        if (search === _arrWords[y] || iconDo.includes(search) || iconDont.includes(search)) {
          upadateResponse(i)
        }
      }
    }
  }

  /* ==========================================================================
    Call the filter function for each word in querry
    ========================================================================== */
  multipleWordsQuery.map((word) => {
    filterIcons(word)
  })

  /* ==========================================================================
     If the search by name/tag return no match, throw a 404 error.
     ========================================================================== */
  if (_response.length === 0) return res.status(404).json({ error: `No results found` })

  /* ==========================================================================
     Filter the _response looking for duplicates.
     ========================================================================== */
  /**
   * Since the tag filter can duplicate the array because more than one tag
   * contain the same keyword, before we send the array we clear the duplicates.
   * https://stackoverflow.com/questions/2218999/remove-duplicates-from-an-array-of-objects-in-javascript
   */

  const response = _response.filter((tag, index, self) =>
    index === self.findIndex((t) => (
      t.place === tag.place && t.name === tag.name
    ))
  )

  res.status(201).json(response)
})

module.exports = router
